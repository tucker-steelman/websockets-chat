# websockets-chat #
## Real-time chat in Node.js built with socket.io ##

### The master branch contains the most up-to-date working code ###

### How do I get set up? ###

1. Install Node.js (https://nodejs.org/en/download/)
2. Clone the repository or download the source code
3. Run 'node index.js' in your terminal in the project root
4. The project will be hosted on http://localhost:3000/