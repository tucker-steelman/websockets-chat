var socket = io();

// Changes the user's nickname
$('#name-form').submit(function(e) {
	e.preventDefault();
	socket.emit('name change', $('.chat__name-field').val());
});

// Submits a new message to the chat box
$('#input-form').submit(function(e) {
	e.preventDefault();
	socket.emit('message', $('.chat__input-field').val());
	$('.chat__input-field').val('');
});

// Prints a welcome message
socket.on('connection', function() {
	printAsChatConsole("Welcome to websockets-chat");
	printAsChatConsole("Type '/help' for a list of commands");
});

// Prints a new connection notification
socket.on('new connection', function(data) {
	printAsChatConsole(data.name + " has joined the chat");
	bottom();
});

// Prints a chat message
socket.on('message', function(data) {
	$('.chat__text-field').append('<div class="message"><span class="message__timestamp">' + getShortTimestamp() + ' ~ ' + '</span>' + '<b>' + data.name + '</b>: ' + data.message + '</div>');
	bottom();
});

// Prints a name change notification
socket.on('name change', function(data) {
	printAsChatConsole(data.previousName + " has changed their name to " + data.newName);
	bottom();
});

// Prints a disconnect notification
socket.on('disconnect', function(data) {
	printAsChatConsole(data.name + " has left the chat");
	bottom();
});

// Gets a UTC timestamp in the format HH:MM
function getShortTimestamp() {
	var date = new Date();
	var hour = ('0' + date.getUTCHours().toString()).slice(-2);
	var minute = ('0' + date.getUTCMinutes().toString()).slice(-2);
	return hour + ":" + minute;
}

// Gets a UTC timestamp in the format MM/DD/YYYY HH:MM
function getFullTimestamp() {
	var date = new Date();
	var month = ('0' + date.getMonth().toString()).slice(-2);
	var day = ('0' + date.getDate().toString()).slice(-2);
	var year = date.getFullYear();
	return month + "/" + day + "/" + year + " " + getShortTimestamp();
}

// Submits a new message to the chat box
// as the chat console
function printAsChatConsole(text) {
	$('.chat__text-field').append('<div class="message"><span class="message__timestamp">' + getShortTimestamp() + ' # ' + '</span>' + '<em>' + text + "</em></div>");
}

// Scroll to the bottom of the page
function bottom() {
	window.scrollTo(0,document.body.scrollHeight);
}
