// Prints list of commands
socket.on('help', function() {
  printAsChatConsole("/clear - clear the screen");
  printAsChatConsole("/time - get the time");
  printAsChatConsole("/users - get the users online");
  printAsChatConsole("/roll - roll a dice");
  bottom();
});

// Clears the screen
socket.on('clear', function(data) {
	$('.chat__text-field').html("");
});

// Prints a UTC timestamp
socket.on('get time', function(data) {
	printAsChatConsole(data.name + ", the time is " + getFullTimestamp());
	bottom();
});

// Prints the users online
socket.on('get users', function(data) {
	printAsChatConsole(data.list.length.toString() + " user(s) online");
	printAsChatConsole(data.list);
	bottom();
});

// Rolls a dice
socket.on('roll', function(data) {
	var dice = Math.floor(Math.random() * 6) + 1;
	printAsChatConsole(data.name + " rolled a " + dice.toString());
	bottom();
});
