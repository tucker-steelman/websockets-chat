var express = require('express')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var sanitize = require('sanitize-html');

http.listen(3000);

// Used to create a unique socket name for the
// default username (guest)
var id = 1;

// Array to hold the user list
var users = [];

// Routing
app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html');

	app.use(express.static(__dirname + '/css'));
	app.use(express.static(__dirname + '/js'));
});

io.on('connection', function(socket) {
	// Default name unless changed by user
	socket.name = "guest" + id.toString();
	id++;
	users.push(socket.name);

	socket.emit('connection');
	io.emit('new connection', {name: socket.name});

	// Send message
	socket.on('message', function(data){
		data = sanitize(data);

		if (data.charAt(0) === '/') {
			getCommand(data, socket);
		} else {
			io.emit('message', {message: data, name: socket.name});
		}
	});

	// Name change
  socket.on('name change', function(data) {
  	data = sanitize(data);

  	// Scan array of users and change this users name
  	for (var i in users) {
  		if (users[i] === socket.name) {
  			users[i] = data;
  		}
  		// prevent overlapping names if name already exists
  		else if (users[i] === data) {
  			return;
  		}
  	}

  	io.emit('name change', {previousName: socket.name, newName: data});
  	socket.name = data;
  });

	// Closing socket/disconnect
	socket.on('disconnect', function(data) {
		// Remove the name from the users list
		for (var i in users) {
			if (users[i] === socket.name) {
				users.splice(1, i);
			}
		}

		io.emit('disconnect', {name: socket.name});
	});
});

// Finds which chat command to execute based on
// the message that the user types
function getCommand(data, socket) {
	switch(data) {
		// Prints a list of the chat commands
		case "/help":
			io.emit('help');
			break;

		// Clears the screen
		case "/clear":
			io.emit('clear');
			break;

		// Prints the clients UTC timestamp
		case "/time":
			io.emit('get time', {name: socket.name});
			break;

		// Prints the names of all the users in the chat
		case "/users":
			io.emit('get users', {list: users});
			break;

		// Rolls a dice
		case "/roll":
			io.emit('roll', {name: socket.name});
			break;

		default: return;
	}
}
